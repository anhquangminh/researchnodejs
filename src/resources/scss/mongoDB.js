const { MongoClient } = require("mongodb");

// Replace the uri string with your MongoDB deployment's connection string.
const uri = "mongodb+srv://<user>:<password>@<cluster-url>?retryWrites=true&writeConcern=majority";

const client = new MongoClient(uri);

//insert to mongodb
async function insert() {
    try {
        await client.connect();

        const database = client.db("insertDB");
        const haiku = database.collection("haiku");
        // create a document to insert
        const doc = {
            title: "Record of a Shriveled Datum",
            content: "No bytes, no problem. Just insert a document, in MongoDB",
        }
        const result = await haiku.insertOne(doc);

        console.log(`A document was inserted with the _id: ${result.insertedId}`);
    } finally {
        await client.close();
    }
}
insert().catch(console.dir);

//update 
async function update() {
    try {
        await client.connect();
        const database = client.db("sample_mflix");
        const movies = database.collection("movies");
        // create a filter for a movie to update
        const filter = { title: "Random Harvest" };
        // this option instructs the method to create a document if no documents match the filter
        const options = { upsert: true };
        // create a document that sets the plot of the movie
        const updateDoc = {
            $set: {
                plot: `A harvest of random numbers, such as: ${Math.random()}`
            },
        };
        const result = await movies.updateOne(filter, updateDoc, options);
        console.log(
            `${result.matchedCount} document(s) matched the filter, updated ${result.modifiedCount} document(s)`,
        );
    } finally {
        await client.close();
    }
}
update.catch(console.dir);

//delete
async function dl() {
    try {
        await client.connect();
        const database = client.db("sample_mflix");
        const movies = database.collection("movies");
        // Query for a movie that has title "Annie Hall"
        const query = { title: "Annie Hall" };
        const result = await movies.deleteOne(query);
        if (result.deletedCount === 1) {
            console.log("Successfully deleted one document.");
        } else {
            console.log("No documents matched the query. Deleted 0 documents.");
        }
    } finally {
        await client.close();
    }
}
dl().catch(console.dir);